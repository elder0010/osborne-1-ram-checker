write "memt.com"

screen_memory equ &f000 ;&c000 ;&f000 osborne
memory_start equ &0220
memory_end equ &caff
rom_start equ &cb00 ;use cb00 for osborne / bfc0 for amstrad

org &100
;org &8000

start:
;-----------------------------------------------------
;Clear screen
	ld a,#20
	ld (screen_memory),a
	ld hl,screen_memory
	ld de,screen_memory+1
	ld bc,#1000
	ldir
;-----------------------------------------------------
;Addressing test (main loop)
addressing_t_start:
	;set the number of bytes to copy
	ld a,(bank_pt)	
	ld hl,bank_len
	call ld_hl_by_table_index ;set hl = (bank_len+bank_pt)
	ld(cur_ram_amt),hl
	;set the copy destination
	ld a,(bank_pt)
	ld hl,bank_start
	call ld_hl_by_table_index ;set hl = (bank_start+bank_pt)
	ld (cur_ram_start),hl	
	ex de,hl ;de = cur_ram_start
	ld hl,rom_start
bytes_amt:
	ld bc,(cur_ram_amt)
	ldir	;copy from ROM to RAM
	call verify_rom_block
	ld a,(bank_pt)
	add #2
	ld (bank_pt),a
	cp #4e
	jp nz, addressing_t_start
addressing_t_end:
;-----------------------------------------------------
;Single byte Test (main loop)
	ld hl,memory_start
	push hl
prep_next_byte:
	pop hl
	ld a,#0 
	ld (hl),a ;resets the byte to test
	call test_byte
 	inc hl
	push hl
	ld de,memory_end+1
	sbc hl,de
	jp nz, prep_next_byte
all_ok:
	ret

;-----------------------------------------------------
;Set hl with an indexed value from a table
;input hl = table address
;input a = table index 
ld_hl_by_table_index:
	ld d,0
	ld e,a 
	add hl,de ;add indirection (a=table address+table index)
	ex hl,de
	ld a,(de)
	ld l,a	;fetch lo byte
	inc de
	ld a,(de) ;fetch hi byte
	ld h,a	;finally hl = address+table index
	ret

;-----------------------------------------------------
;Check that copied data matches with ROM
verify_rom_block:
	ld de,(cur_ram_amt)
	ld hl,#0000
	ld (w_counter),hl
	;ld hl,(cur_ram_start)
	;ld (tmp_w),hl
	ld hl,rom_start
	ld (tmp_w2),hl
next_byte:
	ld hl,(cur_ram_start)
	push hl
	push de
	ld de,screen_memory
	call num2hex
	pop de
	pop hl
	ld a,(hl) ;load a with tmp_w
	inc hl
	ld (cur_ram_start),hl ;store incremented tmp_w for next iteration
	ld hl,(tmp_w2)
	ld b,(hl) ;load b with tmp_2
	inc hl
	ld (tmp_w2),hl ;store incremented tmp_w2 for next iteration
	cp a,b
	jp nz, error
	ld hl,(w_counter)
	inc hl
	ld (w_counter),hl
	sbc hl,de
 	jp nz,next_byte
	ret

;-----------------------------------------------------
;Tests a single byte on values 0->255
test_byte:
	ld a,0 ;comparator counter
test_again:
	;ld (tmp_w),hl ;store hl
	ld (tmp_a),a ;store a
	push hl
	ld de,screen_memory
	call num2hex
	ld a,(tmp_a)	;restore a
	;ld hl,(tmp_w)	;restore hl
	pop hl
	cp a,(hl) ;cmp a with the current byte
	jp nz,error ;no match? -> error!
	inc (hl) ;increment the byte value for the next test
	inc a	;increment the a register for the next test	
	jp nz, test_again
	ret

;-----------------------------------------------------
;Error handling (todo)
error:
	jr error

;-----------------------------------------------------
;Convert 16bit unsigned hex to ASCII
;input hl -> 16bit value to write
;input de -> address of the ASCII string (4 bytes)
num2hex:
	ld a,h
	call num1
	ld a,h
	call num2
	ld a,l
	call num1
	ld a,l
	jr num2
num1:	
	rra
	rra
	rra
	rra
num2:	
	or #f0
	daa
	add a,#a0
	adc a,#40
	ld (de),a
	inc de
	ret
;-----------------------------------------------------
;Variables
tmp_w:
defb 0,0

tmp_w2:
defb 0,0

w_counter:
defb 0,0

tmp_a:
defb 0

bank_pt:
defb 0

cur_ram_start:
defw 0

cur_ram_amt:
defw 0

bank_start:
defw #0280,#0720,#0c20,#1120,#1620,#2020,#2520,#2a20,#2f20,#3420,#3e20,#4320,#4820,#4d20,#5220,#5720,#5c20,#6120,#6620,#6b20,#7020 ;osborne
defw #7520,#7a20,#7f20,#8420,#8920,#8e20,#9320,#9820,#9d20,#a220,#a720,#ac20,#b120,#b620,#bb20,#c020,#c520,#ca20

;defw #9000,#9200 ;amstrad

bank_len:
defw #0500,#0500,#0500,#0500,#0500,#0500,#0500,#0500,#0500,#0500,#0500,#0500,#0500,#0500,#0500,#0500,#0500,#0500,#0500,#0500,#0500 ;osborne
defw #0500,#0500,#0500,#0500,#0500,#0500,#0500,#0500,#0500,#0500,#0500,#0500,#0500,#0500,#0500,#0500,#0500,#00df

;defw #0020 ,#0020 ;amstrad
